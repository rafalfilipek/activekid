"""empty message

Revision ID: 12e1f2e8966e
Revises: 35bbd51a0a34
Create Date: 2013-12-08 10:09:19.406480

"""

# revision identifiers, used by Alembic.
revision = '12e1f2e8966e'
down_revision = '35bbd51a0a34'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    #op.add_column('activity', sa.Column('child', sa.Integer(), nullable=True))
    #op.add_column('activity', sa.Column('date', sa.DateTime(), nullable=True))
    op.add_column('activity', sa.Column('points', sa.Integer(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('activity', 'points')
    #op.drop_column('activity', 'date')
    #op.drop_column('activity', 'child')
    ### end Alembic commands ###
