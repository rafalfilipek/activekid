"""empty message

Revision ID: 103c15c6a293
Revises: 21a10012409a
Create Date: 2013-12-07 18:04:20.513051

"""

# revision identifiers, used by Alembic.
revision = '103c15c6a293'
down_revision = '13b9391f99d4'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('activity', sa.Column('date', sa.DateTime(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('activity', 'date')
    ### end Alembic commands ###
