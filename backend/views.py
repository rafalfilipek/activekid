import json
from datetime import datetime, timedelta

from flask import abort, request
from flask.ext.restful import Resource

from sqlalchemy import and_

import database
import pprint


def authenticate(f):
    def wrapped(self, *args, **kwargs):
        try:
            token = request.values['token']
        except KeyError:
            abort(403)

        parent = database.Parent.verify_auth_token(token)

        if parent:
            return f(self, parent, *args, **kwargs)

        abort(403)

    return wrapped


class RegisterView(Resource):

    def post(self):
        try:
            email = request.json['email']
            password = request.json['password']
        except KeyError:
            abort(500)

        if database.Parent.register(email, password):
            return {'success': True}

        abort(500)


class LoginView(Resource):

    def post(self):
        try:
            email = request.json['email']
            password = request.json['password']
        except KeyError:
            abort(500)

        user = database.Parent.query.filter_by(email=email).first()
        if user and user.check_password(password):
            token = user.generate_auth_token()
            return {'token': token.decode('ascii')}

        return {'error': 'incorrect login/pass'}


class ActivityView(Resource):

    # @authenticate
    def get(self):
        activity_id = request.values.get('id', None)

        if activity_id:
            activity = database.Activity.query.get(activity_id)
            return activity.to_json()

        activities = database.Activity.query.all()
        return [activity.to_json for activity in activities]

    def post(self):
        activities = request.json['activities']
        calories = request.json['goals']['calories']

        activity = database.Activity(calories)
        database.db.session.add(activity)
        database.db.session.commit()

        return {'status': 'ok'}


class FitBitActivity(Resource):

    def post(self):
        calories = request.json.get('calories')
        # steps = request.values['steps']
        email = request.json.get('email')
        # active_minutes = request.values['active_minutes']

        d = datetime.now().strftime('%Y-%m-%d')
        d2 = datetime.now() + timedelta(days=1)
        dd2 = d2.strftime('%Y-%m-%d')
        print "Do {0} do {1}".format(d, dd2)

        child = database.Child.query.filter_by(
            email=email
        ).first()

        today_activities = database.Activity.query.filter(
            and_(database.Activity.child == child.id,
                 database.Activity.date >= d,
                 database.Activity.date <= dd2)
        ).all()

        sum_calories = sum(act.calories for act in today_activities)

        new_calories = calories - sum_calories

        points = new_calories * 10

        if new_calories > 0:
            activity = database.Activity(1, new_calories)
            activity.date = datetime.now()
            activity.calories = new_calories
            activity.points = points
            database.db.session.add(activity)
            database.db.session.commit()

        return {'status': 'ok'}


class ChildrenView(Resource):

    def get(self):
        children = database.Child.query.filter_by(id=1).all()
        return [child.to_json() for child in children]

    def post(self):
        data = request.json
        args = (
            1,
            data['device_id'],
            data['name'],
            data['email'],
            data['hour_limit'],
            data['session_points_required']
        )

        database.Child.add_child(*args)


class AllChildrenView(Resource):

    def get(self):
        children = database.Child.query.all()
        return [child.to_json() for child in children]

    def post(self):
        status = 'ok'

        data = json.loads(request.data)
        name = data['name']

        return {'status' : status}


class CanIHas(Resource):
    
    def get(self):
        child_id = request.values['child']
        child = database.Child.query.get(child_id)

        if not child:
            abort(404)

        return {'status': 'ok', 'value': child.can_i_use()}


class FunSessionView(Resource):

    def get(self):
        child_id = request.values.get('id', None)

        if child_id:
            fun_sessions = database.FunSession.query.filter(
                and_(database.FunSession.child == child_id)
            ).last()

            if fun_sessions:
                return fun_session.to_json()

        return {'status': 'Dzidzia (%s) sie zgubila.' % child_id}

    def post(self):
        child_id = request.json.get('child_id', None)
        if not child_id:
            return {'status': 'Dzidzia (%s) sie zgubila.' % child_id}

        action = request.json.get('action', None)
        if not action:
            return {'status': 'error', 'msg': 'Action param missing (start|stop)'}
        
        result = False
        if action == 'start':
            fun = database.FunSession(child_id)
            fun.start_date = datetime.now()
            fun.end_date = None
            database.db.session.add(fun)
            database.db.session.commit()
            result = True

        elif action == 'stop':
            fun = database.FunSession.query.filter(
                        and_(
                            database.FunSession.child == child_id,
                            database.FunSession.end_date == None
                        )
                    ).first()
            fun.end_date = datetime.now()
            database.db.session.commit()
            result = True

        return {'status': 'Success' if result is True else 'Error'}
