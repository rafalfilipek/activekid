from flask.ext.script import Command, Option

import database


class CreateParent(Command):

    option_list = (
        Option('--email', '-e', dest='email'),
        Option('--password', '-p', dest='password')
    )

    def run(self, email, password):
        database.Parent.register(email, password)
