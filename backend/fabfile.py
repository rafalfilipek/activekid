from fabric.api import local


def migrate():
    local('python api.py db upgrade')


def serve():
    migrate()
    local('python api.py runserver')
