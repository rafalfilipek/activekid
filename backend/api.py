# -*- coding: utf-8 -*-

from flask import Flask
from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.restful import Api
from flask.ext.script import Manager

import commands
import database
import views


app = Flask(__name__)
api = Api(app)


app.config['SECRET_KEY'] = database.SECRET_KEY


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
database.db.init_app(app)
migrate = Migrate(app, database.db)


api.add_resource(views.LoginView, '/api/login/')
api.add_resource(views.RegisterView, '/api/register/')
api.add_resource(views.ActivityView, '/api/activity/')
api.add_resource(views.AllChildrenView, '/api/child/')
api.add_resource(views.ChildrenView, '/api/my_children/')
api.add_resource(views.CanIHas, '/api/can_i_has/')
api.add_resource(views.FitBitActivity, '/api/activities/fitbit')
api.add_resource(views.FunSessionView, '/api/funsession/')

manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('create_parent', commands.CreateParent)


if __name__ == '__main__':
    manager.run()
