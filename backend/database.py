# -*- coding: utf-8 -*-

from itsdangerous import (
    TimedJSONWebSignatureSerializer as Serializer,
    BadSignature, SignatureExpired)
from werkzeug.security import check_password_hash, generate_password_hash
from sqlalchemy.exc import IntegrityError

from flask.ext.login import UserMixin
from flask.ext.sqlalchemy import SQLAlchemy

from datetime import datetime

SECRET_KEY = ('1dda2eb9c0a6d113a785611ac39e2cf4'
              '221a5da194a79ec1b51e6bb426ce0b2e')


db = SQLAlchemy()


class Parent(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(66))

    def __init__(self, email, password):
        self.email = email
        self.set_password(password)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    @classmethod
    def register(cls, email, password):
        parent = cls(email, password)
        db.session.add(parent)

        try:
            db.session.commit()
        except IntegrityError:
            return False

        return True

    def generate_auth_token(self, expires_in=1000):
        serializer = Serializer(SECRET_KEY, expires_in=expires_in)
        return serializer.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token, expires_in=1000):
        serializer = Serializer(SECRET_KEY, expires_in=expires_in)

        try:
            data = serializer.loads(token)
        except BadSignature:
            return None
        except SignatureExpired:
            return None

        return Parent.query.get(data['id'])

class FunSession(db.Model):

    u"""Klasa reprezentujaca czas na rozrywke.
    Przechowuje czas rozpoczecia sessji i swoj czas zakonczenia.

    W przypadku wykrycia aktywnosci w trakcie FunSession:
         * przerywamy FunSession
         * rozpoczynamy pomiar aktywnosci

    W przypadku wykrycia powrotu do komputera rozpoczynamy nowe FunSession.
    """

    id = db.Column(db.Integer, primary_key=True)

    # data rozpoczecia ostatniej sesji rozrywek
    start_date = db.Column(db.DateTime)

    # data zakonczenia sesji rozrywek
    end_date = db.Column(db.DateTime)

    child = db.Column(db.Integer, db.ForeignKey('child.id'))

    def __init__(self, child_id):
        self.child = child_id

    def to_json(self):
        return {
            'id': self.id,
            'start_date': self.start_date,
            'end_date': self.end_date,
            'calories': self.child
        }


class Child(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    parent = db.Column(db.Integer, db.ForeignKey('parent.id'))
    device_id = db.Column(db.String(255))
    name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    foauth_pass = db.Column(db.String(255))

    # maksymalny czas jaki dziecko może spędzić
    # w czasie jednej sesji na rozrywkach
    hour_limit = db.Column(db.Integer)

    # ilosc punktow, ktore dziecko musi zdobyc aby
    # aktywowac kolejna sesje
    session_points_required = db.Column(db.Integer)

    # relationship fields
    activities = db.relationship(
        'Activity', backref='Child', lazy='dynamic')

    def __init__(self, parent, device_id, name, email, hour_limit):
        self.parent = parent
        self.device_id = device_id
        self.name = name
        self.email = email
        self.hour_limit = hour_limit

    def can_i_use(self):
        """Funkcja przeliczajaca liczbe zdobytych punktow.
        Musimy przeliczyc ile bylo aktywnosci od ostatniego
        FunSession.
        """
        last_fun_session = FunSession.query.order_by(
            FunSession.start_date.desc()).first()

        if not last_fun_session:
            return True

        if last_fun_session.end_date == None:
            last_fun_session.end_date = datetime.now()
        
        activities = self.activities.filter(
            Activity.date >= last_fun_session.end_date).all()

        points_sum = sum(
            [activity.points for activity in activities])

        return points_sum >= self.session_points_required

    @classmethod
    def add_child(cls, parent, device_id, name, email, hour_limit):
        child = cls(parent.id, device_id, name, email, hour_limit)
        db.session.add(child)

        try:
            db.session.commit()
        except IntegrityError:
            return False

        return True

    def to_json(self):
        return {
            'id': self.id,
            'device_id': self.device_id,
            'name': self.name,
            'email': self.email,
            'hour_limit': self.hour_limit,
            'foauth_pass': self.foauth_pass
        }


class Device(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    data = db.Column(db.String(255))
    type = db.Column(db.Enum('fitbit'))

    def __init__(self):
        pass
        self.name = name
        self.hour_limit = hour_limit


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)

    def __init__(self, name):
        self.name = name


class Activity(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    category = db.Column(db.Integer, db.ForeignKey('category.id'))
    calories = db.Column(db.Integer)
    date = db.Column(db.DateTime)
    child = db.Column(db.Integer, db.ForeignKey('child.id'))

    # zunifikowana jednostka porownawcza dla aktywnosci
    # po zdobyciu odpowiedniej liczby punktow mozemy rozpoczac nowa aktywnosc
    points = db.Column(db.Integer)

    def __init__(self, category, calories):
        self.category = category

    def to_json(self):
        return {
            'id': self.id,
            'category': self.category,
            'calories': self.calories
        }
