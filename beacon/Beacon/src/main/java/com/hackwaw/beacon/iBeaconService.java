package com.hackwaw.beacon;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.radiusnetworks.ibeacon.IBeacon;
import com.radiusnetworks.ibeacon.IBeaconConsumer;
import com.radiusnetworks.ibeacon.IBeaconManager;
import com.radiusnetworks.ibeacon.MonitorNotifier;
import com.radiusnetworks.ibeacon.RangeNotifier;
import com.radiusnetworks.ibeacon.Region;

import java.util.Collection;

public class iBeaconService extends Service implements IBeaconConsumer {
    private static final String TAG = "iBeaconService";
    private static final String REGION_ID = "DEFAULT_REGION_ID";
    private static final String ACTION_START = "iBeaconService/ACTION_START";
    private static final String ACTION_STOP = "iBeaconService/ACTION_STOP";
    private IBeaconManager iBeaconManager = IBeaconManager.getInstanceForApplication(this);
    private boolean isConnected = false;
    private boolean doMonitoring = false;
    private Region defaultRegion = new Region(REGION_ID, null, null, null);

    public iBeaconService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
        iBeaconManager.bind(this);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
        iBeaconManager.unBind(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand action=" + intent.getAction());

        if (ACTION_START.equals(intent.getAction())) {
            startMonitoring();
        } else if (ACTION_STOP.equals(intent.getAction())) {
            try {
                iBeaconManager.stopMonitoringBeaconsInRegion(defaultRegion);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            stopSelf();
        }
        return START_STICKY;
    }

    public static void start(Context ctx) {
        Intent intent = new Intent(ctx, iBeaconService.class);
        intent.setAction(ACTION_START);
        ctx.startService(intent);
    }

    public static void stop(Context ctx) {
        Intent intent = new Intent(ctx, iBeaconService.class);
        intent.setAction(ACTION_STOP);
        ctx.startService(intent);
    }

    private void startMonitoring() {
        doMonitoring = true;

        if (! isConnected) {
            return;
        }

        try {
            iBeaconManager.startMonitoringBeaconsInRegion(defaultRegion);
        } catch (RemoteException exc) {
            //
        }
    }

    @Override
    public void onIBeaconServiceConnect() {
        final Context ctx = this;

        iBeaconManager.setRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<IBeacon> iBeacons, Region region) {
//                Log.i(TAG, "The first iBeacon I see is about "+iBeacons.iterator().next().getAccuracy()+" meters away.");

                for (IBeacon iBeacon: iBeacons) {
                    Log.d(TAG, "==> " + iBeacon);
                    BackendService.send(ctx, iBeacon.getMajor(), iBeacon.getMinor(), iBeacon.getProximity(), iBeacon.getProximityUuid(), iBeacon.getRssi());
                    break;
                }

                stopSelf();
            }
        });

        try {
            iBeaconManager.startRangingBeaconsInRegion(new Region(REGION_ID, null, null, null));
        } catch (RemoteException e) {
            // pass
        }

        /*
        iBeaconManager.setMonitorNotifier(new MonitorNotifier() {
            @Override
            public void didEnterRegion(Region region) {
                Log.d(TAG, "didEnterRegion: region=" + region);
                assert null != null;
            }

            @Override
            public void didExitRegion(Region region) {
                Log.d(TAG, "didExitRegion: region=" + region);
                assert null != null;
            }

            @Override
            public void didDetermineStateForRegion(int i, Region region) {
                Log.d(TAG, "didDetermineStateForRegion: i=" + i + ", region=" + region);
                assert null != null;
            }
        });
         */


        isConnected = true;

        if (doMonitoring) {
            startMonitoring();
        }
    }
}
