package com.hackwaw.beacon;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import retrofit.RestAdapter;

public class BackendService extends IntentService {
    private static final String TAG = "BackendService";

    public BackendService() {
        super("BackendService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setServer("http://37.233.98.53")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        int major = intent.getIntExtra("major", -1);
        int minor = intent.getIntExtra("minor", -1);
        int proximity = intent.getIntExtra("proximity", -1);
        String proximityUuid = intent.getStringExtra("proximityUuid");
        int rssi = intent.getIntExtra("rssi", -1);

        try {
            BackendAPI backendAPI = restAdapter.create(BackendAPI.class);
            backendAPI.beaconInRange(major, minor, proximity, proximityUuid, rssi);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public static void send(Context ctx, int major, int minor, int proximity, String proximityUuid, int rssi) {
        Log.d(TAG, "=====================");
        Intent intent = new Intent(ctx, BackendService.class);
        intent.putExtra("major", major);
        intent.putExtra("minor", minor);
        intent.putExtra("proximity", proximity);
        intent.putExtra("proximityUuid", proximityUuid);
        intent.putExtra("rssi", rssi);
        ctx.startService(intent);
    }
}
