package com.hackwaw.beacon;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by alek on 08/12/13.
 */
public interface BackendAPI {
    @FormUrlEncoded
    @POST("/api/beacons")
    public Response beaconInRange(
            @Field("major") int major,
            @Field("minor") int minor,
            @Field("proximity") int proximity,
            @Field("proximityUuid") String proximityUuid,
            @Field("rssi") int rssi
    );
}
