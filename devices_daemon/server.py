import requests
import time
from datetime import datetime
import json
import pprint

child = { 'child' : '1' }

DISTANCE = 'http://37.233.98.53/hw/distance'
LAST_SEEN = 'http://37.233.98.53/hw/last_seen'
CAN_I = 'http://37.233.98.53/api/can_i_has/'
TV_ON = 'http://37.233.98.53/hw/on/socket'
TV_OFF = 'http://37.233.98.53/hw/off/socket'
FUNSESSION = 'http://37.233.98.53/api/funsession/'

time_out = 1
last_status = ''
last_can_i = False

def use_computer():
    global last_status
    try:
        data_distance = json.loads(requests.get(DISTANCE).text)
    except: 
        pprint.pprint('error api data_distance')
        
    try:
        data_last_seen = json.loads(requests.get(LAST_SEEN).text)
    except: 
        pprint.pprint('error api data_last_seen' )
    
    try:
        pprint.pprint('dystans: ' + str(data_distance['distance']) + ' | last_seen ' + str(data_last_seen['last_seen_seconds_ago']))
        if (data_distance['distance'] < 0.5 or data_last_seen['last_seen_seconds_ago'] < 30) and last_status != 'use':
            send_use_computer()
            last_status = 'use'
        
        if data_distance['distance'] > 0.5 and data_last_seen['last_seen_seconds_ago'] > 30 and last_status == 'use':
            send_out()
            last_status = 'out'
    except: 
        pass

def send_use_computer():
    pprint.pprint('use')
    try:
        data = {  "child_id": "1",  "action": "start" }
        requests.post(FUNSESSION, data=json.dumps(data))
    except:
        pass

def send_out():
    pprint.pprint('out')
    try:
        data = {  "child_id": "1",  "action": "stop" }
        requests.post(FUNSESSION, data=json.dumps(data))
    except:
        pass

def can_i_on_off():
    global last_can_i
    try:
        data_can_i = json.loads(requests.get(CAN_I , params=child).text)
        if bool(data_can_i['value']):
            pprint.pprint('ON')
        else:
            pprint.pprint('OFF')
        if last_can_i != bool(data_can_i['value']):
            on_off(data_can_i['value'])
        last_can_i = bool(data_can_i['value'])
    except: 
        pprint.pprint('Error can_i_on_off')

def on_off(decision):
    if decision == False:
        requests.get(TV_ON)
    elif decision == True:
        requests.get(TV_ON)
while True:
    use_computer()
    can_i_on_off()
    time.sleep(5)
