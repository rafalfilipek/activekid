app.factory('api', ($http, $rootScope, $cookieStore)->
	apiUrl = '/api/'
	{
		'get': (path, params = {})->
			params['token'] = $rootScope.token
			paramsString = (key + '=' + val for key, val of params).join('&')
			$http.get(apiUrl + path + '?' + paramsString).error(->
				# $rootScope.token = undefined
				# $cookieStore.remove('token')
			)
		'post': (path, data)->
			$http.post(apiUrl + path, data).error(->
				# $rootScope.token = undefined
				# $cookieStore.remove('token')
			)
	}
)
