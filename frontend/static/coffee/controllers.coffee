LoginCtrl = ($scope, $rootScope, api, $cookieStore)->
	$scope.login = ->
		if $scope.email is undefined or $scope.password is undefined
			alert('WTF dude?')
			return false
		else
			api.post('login/', {
				'email': $scope.email,
				'password': $scope.password
			}).success((response)->
				if response.token
					$rootScope.token = response.token
					$cookieStore.put('token', response.token)
				else
					alert('Zły login lub hasło.')
			).error(->
				alert('Zły login lub hasło.')
			)

RegisterCtrl = ($scope, $rootScope, api, $location)->
	$scope.register = ->
		if $scope.email is undefined or $scope.password is undefined
			alert('WTF dude?')
			return false
		else
			api.post('register/', {
				'email': $scope.email,
				'password': $scope.password
			}).success((response)->
				if response.success
					$location.path('/login')
				else
					alert('Coś jest nie tak.')
			).error(->
				alert('Coś jest nie tak.')
			)

LogoutCtrl = ($scope, $rootScope, $cookieStore, $location)->
	$rootScope.token = undefined
	$cookieStore.remove('token')
	$location.path('/login')


MainCtrl = ($scope, api)->
	api.get('my_children/').success((rows)->
		api.get('activity/', {'id': rows[0].id}).success((response)->
			console.log response
		)
	)
	# $scope.word = 'Hello'
	$scope.chartConfig = {
		options: {
			chart: {
				type: 'bar'
			}
		},
		series: [{
			data: [10, 15, 12, 8, 7]
		}],
		title: {
			text: 'Hello'
		},
		loading: false,
		xAxis: {
			currentMin: 0,
			currentMax: 20,
			title: {text: 'values'}
		},
		useHighStocks: false
	}

	$scope.update = ->
		$scope.chartConfig.series = [{
			data: [1,2,3,4,5]
		}]
