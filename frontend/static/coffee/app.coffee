app = angular.module('app', ['ngAnimate', 'ngResource', 'ngRoute', 'ngCookies', 'highcharts-ng'])

app.config(($routeProvider)->
	$routeProvider
		.when('/', {
			'controller': 'MainCtrl'
			'templateUrl': 'views/index.html'
		})
		.when('/login', {
			'controller': 'LoginCtrl'
			'templateUrl': 'views/login.html'
		})
		.when('/logout', {
			'controller': 'LogoutCtrl'
		})
		.when('/register', {
			'controller': 'RegisterCtrl'
			'templateUrl': 'views/register.html'
		})
		.otherwise({
			'redirectTo': '/'
		})
).run(($rootScope, $location, $cookies)->
	token = $cookies.token
	if token
		$rootScope.token = token
	$rootScope.$watch('token', ->
		if angular.isDefined($rootScope.token)
			$cookies.token = $rootScope.token
			currentPath = $location.path()
			path = if currentPath in ['/login', '/register'] then '/' else currentPath
		else
			path = if currentPath in ['/login', '/register'] then currentPath else '/login'
		$location.path(path)
	)
)
