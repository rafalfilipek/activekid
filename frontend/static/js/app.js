// Generated by CoffeeScript 1.6.3
var app;

app = angular.module('app', ['ngAnimate', 'ngResource', 'ngRoute', 'ngCookies', 'highcharts-ng']);

app.config(function($routeProvider) {
  return $routeProvider.when('/', {
    'controller': 'MainCtrl',
    'templateUrl': 'views/index.html'
  }).when('/login', {
    'controller': 'LoginCtrl',
    'templateUrl': 'views/login.html'
  }).when('/logout', {
    'controller': 'LogoutCtrl'
  }).when('/register', {
    'controller': 'RegisterCtrl',
    'templateUrl': 'views/register.html'
  }).otherwise({
    'redirectTo': '/'
  });
}).run(function($rootScope, $location, $cookies) {
  var token;
  token = $cookies.token;
  if (token) {
    $rootScope.token = token;
  }
  return $rootScope.$watch('token', function() {
    var currentPath, path;
    if (angular.isDefined($rootScope.token)) {
      $cookies.token = $rootScope.token;
      currentPath = $location.path();
      path = currentPath === '/login' || currentPath === '/register' ? '/' : currentPath;
    } else {
      path = currentPath === '/login' || currentPath === '/register' ? currentPath : '/login';
    }
    return $location.path(path);
  });
});
