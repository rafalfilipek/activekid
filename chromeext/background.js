var prev_can_i_use = true;

var work_notifications = [];
var done_notifications = [];

var show_work = function() {
  for (var i=0, len=done_notifications.length; i<len; i++) {
    done_notifications[i].hide();
  }
  done_notifications = [];

  var notification = webkitNotifications.createNotification(
    'icon.png',
    'Ćwiczenia',  // notification title
    'Czas na ćwiczenia GRUBASIE!'  // notification body text
  );

  notification.show();
  work_notifications.push(notification);
};

var show_done = function () {
  for (var i=0, len=work_notifications.length; i<len; i++) {
    work_notifications[i].hide();
  }
  work_notifications = [];

  var notification = webkitNotifications.createNotification(
    'icon-done.png',
    'Ćwiczenia',
    'Wyglądasz super przystojniaku!'
  );
  notification.show();
  var achive_notification = webkitNotifications.createNotification(
    'chuck_active.png',
    'Ćwiczenia',
    'Zdobyłeś osiągnięcie Chuck Noris - za spalenie 10000 kalori!'
  );

  achive_notification.show();

  done_notifications.push(notification);
}

setInterval(function () {

  $.ajax({
    url: 'http://37.233.98.53/api/can_i_has?child=1'
  }).done(function(data) {
    if (data.value === false) {
      show_work();
      prev_can_i_use = false;
      console.log('work stupid!');
    } else if (data.value === true && prev_can_i_use === false) {
      show_done();
      prev_can_i_use = true;
      console.log('work done');
    } else if (data.value === true && prev_can_i_use === true){
      console.log('do nothing');
    }
  })
}, 10 * 1000);
