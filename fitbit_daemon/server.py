import requests
import time
from datetime import datetime
import json

# users = [
#     {"email": "pawel.piotrowski@gmail.com", "pass": "dupa.8@foauth"}
# ]

BACKEND_HOST = 'http://37.233.98.53'
BACKEND_FITBIT_URL = '{0}/api/activities/fitbit'.format(BACKEND_HOST)
BACKEND_USERS_URL = '{0}/api/child/'.format(BACKEND_HOST)


def get_fitbit_data(user):
    d = datetime.now()
    auth = user["email"], user["pass"]
    api_url = 'http://foauth.org/api.fitbit.com/1/user/-/activities/date/{0}.json'.format(d.strftime("%Y-%m-%d"))
    # api_url = 'http://foauth.org/api.fitbit.com/1/user/-/activities/recent.json'
    r = requests.get(api_url, auth=auth)
    try:
        ret = json.loads(r.content)
    except Exception as e:
        print "Nie udalo sie pobrac danych fitbit. error: {0} ({1})".format(e, type(e))
        ret = {}
    return ret


def get_users():
    r = requests.get(BACKEND_USERS_URL)
    data = r.json()
    return [{'email': user['email'], 'pass': user['foauth_pass']} for user in data]


def send_activity(user):
    data = get_fitbit_data(user)
    summary = data.get("summary", {})
    calories = summary.get("caloriesOut", 0)
    steps = summary.get("steps", 0)

    active_minutes = sum([
        int(summary.get("lightlyActiveMinutes", 0)),
        int(summary.get("fairlyActiveMinutes", 0)),
        int(summary.get("veryActiveMinutes", 0))
    ])

    data = {
        "calories": calories,
        "steps": steps,
        "active_minutes": active_minutes,
        "email": user["email"]
    }
    print "Wrzucam aktywnosci dla uzytkownika {0}, calories: {1}".format(user["email"], calories)
    r = requests.post(BACKEND_FITBIT_URL, data=json.dumps(data), headers={'Content-Type': 'application/json'})
    print r.content

while True:
    users = get_users()

    for user in users:
        print 'Sprawdzam uzytkownika email: {0}'.format(user["email"])
        send_activity(user)

    time.sleep(30)
