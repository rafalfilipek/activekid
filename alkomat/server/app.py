import pymongo
import json
from bson.objectid import ObjectId
from bson import json_util
import datatime


from flask import Flask
from flask import jsonify
from flask import request

from pymongo import MongoClient
mongo_conn = MongoClient()
db = mongo_conn.alkomat

app = Flask(__name__)
app.config.from_envvar('SERVICE_CONFIGURATION')

#
# http --json --form  POST http://0.0.0.0:5017/new device_id=1 value=1 datetime='2013-12-07 15:24:10.566426'
#

@app.route('/alkomat/new', methods=['POST'])
def post():
    if request.method == 'POST':
        collection = db.checks
        data = request.json
        data['timestamp'] = datatime.datetime.now()
        print data
        try:
            check_id = collection.insert(data)
            return str(check_id), 200
        except Exception, e:
            return e.message, 400

#
# http GET http://0.0.0.0:5017/all/1
#

@app.route('/alkomat/all/<device_id>', methods=['GET'])
def all(device_id):
    collection = db.checks
    documents = collection.find({'device_id': device_id})
    result = []
    [result.append(d) for d in documents]
    return json.dumps(result, default=json_util.default), 200

#
# http GET http://0.0.0.0:5017/single/52a33296fe55ff07796aaeba
#

@app.route('/alkomat/single/<check_id>', methods=['GET'])
def get(check_id):
    collection = db.checks
    document = collection.find_one({'_id': ObjectId(check_id)})
    return json.dumps(document, default=json_util.default), 200

def main():
    app.run(host=app.config['HOST'], port=app.config['PORT'])

if __name__ == '__main__':
    main()
