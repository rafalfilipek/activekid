#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function

import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

class PowerManagement(object):
    def on(self, socket_name):
        raise NotImplementedError()

    def off(self, socket_name):
        raise NotImplementedError()

    def knows(self, socket_name):
        raise NotImplementedError()


class SimplePowerManagement(PowerManagement):
    def __init__(self, **kwargs):
        """kwargs = name: pin number mapping"""

        self._pinmap = dict(kwargs)
        for pin in self._pinmap.values():
            GPIO.setup(pin, GPIO.OUT, initial=GPIO.HIGH)

    def on(self, name):
        GPIO.output(self._pinmap[name], GPIO.LOW)

    def off(self, name):
        GPIO.output(self._pinmap[name], GPIO.HIGH)

    def knows(self, name):
        return name in self._pinmap


class RFPowerManagement(PowerManagement):
    def __init__(self, **kwargs):
        self._pinmap = dict(kwargs)
        for pins in self._pinmap.values():
            GPIO.setup(pins['on'], GPIO.OUT, initial=GPIO.HIGH)
            GPIO.setup(pins['off'], GPIO.OUT, initial=GPIO.HIGH)

    def click(self, pin):
        GPIO.output(pin, GPIO.LOW)
        time.sleep(0.1)
        GPIO.output(pin, GPIO.HIGH)

    def on(self, name):
        self.click(self._pinmap[name]['on'])

    def off(self, name):
        self.click(self._pinmap[name]['off'])

    def knows(self, name):
        return name in self._pinmap
