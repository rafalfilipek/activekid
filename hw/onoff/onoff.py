#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function

from flask import Flask, jsonify
from power_management import SimplePowerManagement, RFPowerManagement
from distance import Distance

import RPi.GPIO as GPIO
import time
import atexit

TRIG = 21
ECHO = 23


SPM = SimplePowerManagement(tv=24, router=26, white=22)
RFPM = RFPowerManagement(socket={'on': 13, 'off': 15})
PMS = [SPM, RFPM]
LAST_SEEN = 0

DIS = Distance(TRIG, ECHO)

app = Flask(__name__)


@app.route("/hw/<action>/<name>")
# @requires_auth
def on(action, name):
    for pm in PMS:
        if not pm.knows(name):
            continue
        getattr(pm, action)(name)
        return jsonify({name: action})
    return jsonify({'error': 'unknonw ' + name})

@app.route("/hw/last_seen")
def last_seen():
    return jsonify({'last_seen_seconds_ago': int(time.time() - LAST_SEEN)})


@app.route("/hw/distance")
def distance():
    DIS.measure()
    time.sleep(1)
    return jsonify({'distance': DIS.distance})


def seen(s):
    global LAST_SEEN
    LAST_SEEN = time.time()
    for el in ("tv", "router", "white"):
        SPM.on(el)
        time.sleep(0.2)
        SPM.off(el)


def main():
    GPIO.setup(18, GPIO.IN)
    GPIO.add_event_detect(18, GPIO.RISING, seen, 200)
    DIS.measure()
    app.run(host="0.0.0.0", debug=False)


if __name__ == '__main__':
    atexit.register(GPIO.cleanup)
    main()

