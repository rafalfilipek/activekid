#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function

import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)


TRIG = 21
ECHO = 23


class Distance(object):
    def __init__(self, trig, echo):
        self.trig = trig
        self.echo = echo
        self.start = 0
        self.distance = -1

        GPIO.setup(self.trig, GPIO.OUT, initial=GPIO.LOW)  #pylint: disable=E1123
        GPIO.setup(self.echo, GPIO.IN)
        GPIO.add_event_detect(self.echo, GPIO.BOTH, self.up)

    def measure(self):
        self.start = None
        self.click(self.trig)

    def click(self, pin):
        GPIO.output(pin, GPIO.HIGH)
        time.sleep(0.002)
        GPIO.output(pin, GPIO.LOW)

    def up(self, _u):
        if not self.start:
            self.start = time.time()
            return
        dif = time.time() - self.start
        self.start = None
        self.distance = dif * 10000 / 29 / 2
        print(dif, "%2f" % (dif * 10000 / 29 / 2))


def main():
    dis = Distance(TRIG, ECHO)
    while True:
        dis.measure()
        time.sleep(2)
        print(dis.distance)


if __name__ == '__main__':
    main()
